# Robotic Grocery Management

Developed for **Individual Project 2020/21**

Demo Video: https://www.youtube.com/watch?v=dYcuSVV0I98

## Purpose

Implements a workflow for the TIAGo robot to sort groceries placed on a table

TIAGo will visit and label destination tables based on the objects present on them, and will sort groceries from a source table to their respective destinations.

## Pre-requisites

**Note:** Tested on Ubuntu 16.04

- [ROS Kinetic Kame](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- [TIAGo Simulation](http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation)
- [Object Recognition Kitchen](https://wg-perception.github.io/object_recognition_core/install.html#install): Install the `tabletop` pipeline and the ROS packages
- [CouchDB with OR Web UI](https://wg-perception.github.io/object_recognition_core/infrastructure/couch.html#couch-db)
- [sensible_robots/custom_worlds](https://gitlab.com/sensible-robots/custom_worlds): Checkout the `teb-planner` branch
- [sensible_robots/ycb_objects](https://gitlab.com/sensible-robots/ycb_objects): Contains Gazebo-compatible models of the YCB dataset

Cloning:

```sh
cd catkin_ws/src
git clone https://gitlab.com/sc18uu/fyp-grocery-sorter.git fyp
```

Run `setup.sh` to set up the execution, or manually do the following tasks:
1. Update joint tolerances in `tiago_controller_configuration/config/pal-gripper_joint_trajectory_controllers.yaml`:
```diff
  gripper_right_finger_joint:
-   goal: 0.02
+   goal: 0.2
  gripper_left_finger_joint:
-   goal: 0.02
+   goal: 0.2
```
2. Add bundled objects to ORK:
```sh
roscd fyp/objects

rosrun fyp add orange orange.stl
rosrun fyp add soup_can soup_can.stl
rosrun fyp add chips_can chips_can.stl

rosrun fyp db.py setsize orange 0.07 0.07
rosrun fyp db.py setsize soup_can 0.07 0.1
rosrun fyp db.py setsize chips_can 0.08 0.25
```
3. Add world to `custom_worlds`:
```sh
cp $(rospack find fyp)/world/kitchen.world $(rospack find custom_worlds)/worlds
```

## Running
```sh
roslaunch fyp start.launch
# in a separate terminal
rosrun fyp sort_groceries.py
```

## Adding objects
Given a compatible 3D file, you can add it to ORK by running the following commands. The width and height should be specified in meters e.g. 15cm = `0.15`.
```sh
rosrun fyp add <name> <file>
rosrun fyp db.py setsize <name> <width> <height>
```
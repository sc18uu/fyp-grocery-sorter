#!/usr/bin/env bash

source ~/.bashrc

set -Eeuo pipefail

msg() {
  echo >&2 -e "${1-}"
}



check() {
    if $2 &> /dev/null
    then
        msg "- $1 found"
    else
        msg "! $1 not found, quitting"
        exit
    fi

}

checkcommand() {
    if command -v "$2" &> /dev/null
    then
        msg "- $1 found"
    else
        echo "! $1 not found, quitting"
        exit
    fi
}

checkrosdir() {
    if rospack find "$2" &> /dev/null
    then
        msg "- $1 found"
    else
        msg "! $1 not found, quitting"
        exit
    fi
}

msg "Checking pre-requisites..."

checkcommand "ROS" rosrun
checkrosdir "TIAGo packages" tiago_description
checkrosdir "ORK" object_recognition_core
check "CouchDB" "curl -s http://localhost:5984/object_recognition"
checkrosdir "custom_worlds" custom_worlds

if rospack find ycb_objects &> /dev/null
then
    msg "! (optional) ycb_objects found"
else
    msg "! (optional) ycb_objects not found"
fi

msg
msg "Updating joint tolerances..."

cd $(rospack find tiago_controller_configuration)/config
cp pal-gripper_joint_trajectory_controllers.yaml pal-gripper_joint_trajectory_controllers_old.yaml
sed -i 's/0.02/0.2/g' pal-gripper_joint_trajectory_controllers.yaml

msg
msg "Adding bundled objects..."

cd $(rospack find fyp)/objects
rosrun fyp add orange orange.stl &> /dev/null
rosrun fyp add soup_can soup_can.stl &> /dev/null
rosrun fyp add chips_can chips_can.stl &> /dev/null
rosrun fyp db.py setsize orange 0.07 0.07 &> /dev/null
rosrun fyp db.py setsize soup_can 0.07 0.1 &> /dev/null
rosrun fyp db.py setsize chips_can 0.08 0.25 &> /dev/null

msg
msg "Adding kitchen world..."

cp $(rospack find fyp)/world/kitchen.world $(rospack find custom_worlds)/worlds

msg
msg "Done!"
#! /usr/bin/env python

import couchdb
import sys

couch = couchdb.Server()

ork = couch["object_recognition"]
objs = ork.view("objects/all")


def find(name=None):
    if name:
        return [o for o in objs if o.key == name]
    return objs


def show(o):
    print o.id, o.key
    if "w" in o.value:
        print "  size", o.value["w"], o.value["h"]


def setsize(name, w, h):
    o = find(name)
    if len(o) > 0:
        id = o[0].id
        obj = ork[id]
        obj["w"] = w
        obj["h"] = h
        ork.save(obj)
        show(o[0])


def get_objects_info():
    out = {}
    for o in objs:
        out[o.id] = {
            "name": o.key,
            "size": (o.value["w"], o.value["w"], o.value["h"])
        }
    return out


if __name__ == "__main__":
    if len(sys.argv) > 1:
        cmd = sys.argv[1]
        if cmd == "list":
            map(show, find())
        elif cmd == "find":
            map(show, find(sys.argv[2]))
        elif cmd == "setsize":
            name = sys.argv[2]
            w, h = map(float, sys.argv[3:5])
            setsize(name, w, h)
        elif cmd == "map":
            print(get_objects_info())
        else:
            print("put option")

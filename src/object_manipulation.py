from grasp_generator import GraspGenerator

import moveit_commander
import rospy

from actionlib import SimpleActionClient
from tf.transformations import quaternion_from_euler

from geometry_msgs.msg import PoseStamped, Quaternion
from moveit_msgs.msg import MoveItErrorCodes, PickupAction, PickupGoal, PlaceAction, PlaceGoal
from visualization_msgs.msg import Marker, MarkerArray


errors = {}
for k in MoveItErrorCodes.__dict__.keys():
    if k[0] != "_":
        errors[MoveItErrorCodes.__dict__[k]] = k


class Gripper:
    closed = [0.001, 0.001]
    opened = [0.044, 0.044]


class Arm:
    up = (0.4, -0.5,  1.0, 1.57, 0, 0.8)
    holding = up


class ObjectManipulation:
    SCENE_OBJECT = "object"

    def __init__(self):
        self.grasp_generator = GraspGenerator()

        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.arm_torso = moveit_commander.MoveGroupCommander("arm_torso")
        self.gripper = moveit_commander.MoveGroupCommander("gripper")

        self._pickup = SimpleActionClient("/pickup", PickupAction)
        self._place = SimpleActionClient("/place", PlaceAction)

        self._pickup.wait_for_server()
        self._place.wait_for_server()

        self._markers = rospy.Publisher("/locations", MarkerArray)
        self.markers = []

    def add_point(self, point, col=(1, 0, 0), time=1):
        m = Marker()
        m.header.frame_id = "base_footprint"
        m.header.stamp = rospy.Time.now()
        m.ns = "corners"
        m.id = len(self.markers)
        m.type = Marker.SPHERE
        m.action = Marker.ADD
        m.pose.position = point
        m.pose.orientation.w = 1
        m.scale.x = 0.1
        m.scale.y = 0.1
        m.scale.z = 0.1
        m.color.r = col[0]
        m.color.g = col[1]
        m.color.b = col[2]
        m.color.a = 1
        m.lifetime = rospy.Duration(time)
        self.markers.append(m)

    def display_markers(self):
        ms = MarkerArray(self.markers)
        self.markers = []
        self._markers.publish(ms)

    def add_to_scene(self, obj):
        pose = PoseStamped()
        pose.header.frame_id = "base_footprint"
        pose.pose.position = obj["center"].point
        pose.pose.orientation.w = 1

        self.scene.add_box(self.SCENE_OBJECT, pose, size=obj["size"])

    def clear_scene(self):
        self.scene.remove_attached_object("arm_tool_link")
        self.scene.remove_world_object(self.SCENE_OBJECT)

    def grip(self, grip):
        self.gripper.go(grip, wait=True)
        self.gripper.stop()

    def is_gripper_empty(self):
        l, r = self.gripper.get_current_joint_values()
        return l < 0.015 and r < 0.015  # less than 3cm between fingers

    def reset_arm(self):
        self.grip(Gripper.opened)
        self.arm_go_to(*Arm.up)

    def arm_go_to(self, x, y, z, roll=0, pitch=0, yaw=0):
        pose = PoseStamped()
        pose.header.frame_id = "base_footprint"
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation = Quaternion(
            *quaternion_from_euler(roll, pitch, yaw))

        self.arm_torso.set_pose_reference_frame("base_footprint")
        self.arm_torso.set_pose_target(pose)

        self.arm_torso.go(wait=True)
        self.arm_torso.stop()

        self.arm_torso.clear_pose_targets()

    def pick_up(self, obj):
        c = obj["center"]
        object = PoseStamped()
        object.header.frame_id = "base_footprint"
        object.pose.position.x = c.point.x
        object.pose.position.y = c.point.y
        object.pose.position.z = c.point.z + 0.01  # grab slightly higher

        goal = PickupGoal()
        goal.target_name = self.SCENE_OBJECT
        goal.group_name = "arm_torso"
        goal.attached_object_touch_links = [
            "<octomap>", "gripper_link", "gripper_left_finger_link", "gripper_right_finger_link"]
        goal.allowed_planning_time = 20
        goal.planning_options.planning_scene_diff.is_diff = True
        goal.planning_options.planning_scene_diff.robot_state.is_diff = True
        goal.planning_options.plan_only = False
        goal.planning_options.replan = True
        goal.planning_options.replan_attempts = 1

        goal.possible_grasps = self.grasp_generator.create_grasps_from_object_pose(
            object)

        self._pickup.send_goal(goal)
        self._pickup.wait_for_result()
        res = self._pickup.get_result()

        if res.error_code.val == 1:  # Success
            return True
        else:
            rospy.logwarn("Picking up failed with error %s",
                          errors[res.error_code.val])
            return False

    def place(self, object, points):
        poses = []
        for p in points:
            pose = PoseStamped()
            pose.header.frame_id = "base_footprint"
            pose.pose.position.x = p.x
            pose.pose.position.y = p.y
            pose.pose.position.z = p.z + object["size"][2] / 2

            poses.append(pose)

        goal = PlaceGoal()
        goal.attached_object_name = self.SCENE_OBJECT
        goal.group_name = "arm_torso"
        goal.allowed_planning_time = 20
        goal.planning_options.planning_scene_diff.is_diff = True
        goal.planning_options.planning_scene_diff.robot_state.is_diff = True
        goal.planning_options.plan_only = False
        goal.planning_options.replan = True
        goal.planning_options.replan_attempts = 1

        # flatten array of possible placements
        locations = [
            g for p in poses for g in self.grasp_generator.create_placings_from_object_pose(p)]

        goal.place_locations = locations

        self._place.send_goal(goal)
        self._place.wait_for_result()
        res = self._place.get_result()

        if res.error_code.val == 1:  # Success
            self.scene.attach_box("arm_tool_link", self.SCENE_OBJECT,
                                  touch_links=self.robot.get_link_names("gripper"))
            return True
        else:
            rospy.logwarn("Placing failed with error %s",
                          errors[res.error_code.val])
            return False

#! /usr/bin/env python

import sys

import db
from object_recognition import ObjectRecognition
from object_manipulation import ObjectManipulation, Arm
from navigation import Navigation

import moveit_commander
import rospy
import smach
import smach_ros

from geometry_msgs.msg import Point


# TODO: Move these into a YAML file
class Location:
    table = (0.9, 0, 0)
    table2 = (0.3, 0.75, 1.57)
    table3 = (-1, 0.75, 1.57)
    center = (0.3, 0, 1.57)
    bookshelf = (-1.24, 0.85, 1.57)


class Destination:
    def __init__(self, location):
        self.location = location
        self.objects = set()

    def add_object(self, obj):
        self.objects.add(obj["name"])

    def matches(self, obj):
        return obj["name"] in self.objects


def add_vector(point, vector, n=1):
    out = Point()
    out.x = point.x + n * vector.x
    out.y = point.y + n * vector.y
    out.z = point.z + n * vector.z
    return out


def filter_objects(_objs):
    confidence_threshold = 0.85

    objs = _objs
    # keep confident objects located above 40cm
    objs = filter(lambda o: o["confidence"] > confidence_threshold, objs)
    objs = filter(lambda o: o["center"].point.z > 0.4, objs)
    # sort by distance, near to far
    return sorted(objs, key=lambda o: o["center"].point.x)


def filter_tables(_tables):
    tables = _tables
    # keep tables above 40cm
    tables = filter(lambda t: t[0].z > 0.4, tables)
    # sort by height, high to low
    return sorted(tables, key=lambda t: t[0].z, reverse=True)


class Reset(smach.State):
    def __init__(self, is_holding=False):
        smach.State.__init__(self, outcomes=["done"],
                             io_keys=["manipulator", "navigator", "recogniser"])
        self.is_holding = is_holding

    def execute(self, userdata):
        if self.is_holding:
            userdata.manipulator.arm_go_to(*Arm.holding)
        else:
            userdata.manipulator.clear_scene()
            userdata.manipulator.reset_arm()
        userdata.navigator.clear_map()
        return "done"


class CheckDestinations(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser", "destinations"])

    def execute(self, userdata):
        for i, dest in enumerate(userdata.destinations):
            rospy.loginfo("Going to destination %d...", i)
            if not userdata.navigator.go_to(dest.location):
                return "failed"

            # TODO: Only look forward?
            userdata.navigator.update_map()
            objs = filter_objects(userdata.recogniser.get_objects())

            if len(objs) == 0:
                rospy.logwarn("No objects found at this destination!")
                return "failed"

            for o in objs:
                dest.add_object(o)

            rospy.loginfo("Destination %d can hold: %s", i, dest.objects)
        return "succeeded"


class GoDestination(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser", "destinations"], input_keys=["object"])

    def execute(self, userdata):
        # get first matching destination
        i, dest = next(
            ((i, d) for i, d in enumerate(userdata.destinations) if d.matches(userdata.object)), None)

        if not dest:
            rospy.logwarn("No destination registered for %s!",
                          userdata.object["name"])
            return "failed"

        rospy.loginfo("Going to place %s at destination %d...",
                      userdata.object["name"], i + 1)
        if not userdata.navigator.go_to(dest.location):
            return "failed"
        userdata.navigator.update_map()
        return "succeeded"


class GoSource(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser"], input_keys=["source"])

    def execute(self, userdata):
        rospy.loginfo("Going to source...")
        if not userdata.navigator.go_to(userdata.source):
            return "failed"
        userdata.navigator.update_map()
        return "succeeded"


class FindObject(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["no_more_objects", "object_found"],
                             io_keys=["manipulator", "navigator", "recogniser", "destinations"], output_keys=["object"])

    def execute(self, userdata):
        userdata.manipulator.clear_scene()  # added
        objs = filter_objects(userdata.recogniser.get_objects())
	# filter out objects that do not match a destination
        objs = filter(lambda o: any(d.matches(o) for d in userdata.destinations),
	              objs)
	# get first or None
	obj = next(iter(objs), None)

        if obj == None:
            rospy.logwarn("No more objects, quitting...")
            return "no_more_objects"

        userdata.manipulator.add_to_scene(obj)
        userdata.object = obj

        rospy.loginfo("Can see %d object(s)", len(objs))
        rospy.loginfo("Closest object is %s...", obj["name"])
        return "object_found"


class PickObject(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser"], input_keys=["object"])

    def execute(self, userdata):
        obj = userdata.object

        rospy.loginfo("Picking up %s...", obj["name"])
        if userdata.manipulator.pick_up(obj) and not userdata.manipulator.is_gripper_empty():
            userdata.navigator.go_velocity(-0.5, 3)
            userdata.navigator.clear_map()
            userdata.manipulator.arm_go_to(*Arm.holding)

            if userdata.manipulator.is_gripper_empty():
                rospy.logwarn("Failed to pick up %s!", obj["name"])
                userdata.manipulator.reset_arm()  # added
                return "failed"

            return "succeeded"
        else:
            rospy.logwarn("Failed to pick up %s!", obj["name"])
            userdata.manipulator.reset_arm()  # added
            return "failed"


class FindPlace(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser"], output_keys=["place_points"])

    def execute(self, userdata):
        tables = filter_tables(userdata.recogniser.get_tables())

        if len(tables) == 0:
            rospy.logwarn("No tables found!")
            return "failed"

        rospy.loginfo("Can see %d table(s)", len(tables))
        corner, left, forward = tables[0]  # get highest table
        points = []

        def add_at(x):
            for y in range(9, 0, -1):
                p = add_vector(add_vector(
                    corner, left, x), forward, y / 10.0)
                p.z += 0.05
                t = len(userdata.manipulator.markers) / 81.0
                userdata.manipulator.add_point(
                    p, col=(t, 1 - t, 0), time=10)
                points.append(p)

        add_at(0.5)  # add points along the center

        for i in range(1, 5):  # add at increasing distances from center
            add_at(0.5 - i / 10.0)
            add_at(0.5 + i / 10.0)

        # corners
        userdata.manipulator.add_point(corner, col=(0, 0, 1), time=10)
        userdata.manipulator.add_point(add_vector(
            corner, left), col=(1, 1, 1), time=10)
        userdata.manipulator.add_point(add_vector(
            corner, forward), col=(1, 1, 1), time=10)
        userdata.manipulator.add_point(add_vector(add_vector(
            corner, left), forward), col=(1, 1, 1), time=10)

        userdata.manipulator.display_markers()
        userdata.place_points = points
        return "succeeded"


class PlaceObject(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["failed", "succeeded"],
                             io_keys=["manipulator", "navigator", "recogniser", "num_placed"], input_keys=["object", "place_points"])

    def execute(self, userdata):
        rospy.loginfo("Placing object %d: %s...",
                      userdata.num_placed + 1, userdata.object["name"])
        if userdata.manipulator.place(userdata.object, userdata.place_points):
            userdata.num_placed += 1
            return "succeeded"
        else:
            return "failed"


if __name__ == "__main__":
    try:
        rospy.init_node("grocery_sorter")
        moveit_commander.roscpp_initialize(sys.argv)

        sm = smach.StateMachine(
            outcomes=["done_no_more_objects", "done_failed"])

        sm.userdata.navigator = Navigation()
        sm.userdata.manipulator = ObjectManipulation()
        sm.userdata.recogniser = ObjectRecognition(db.get_objects_info())
        sm.userdata.source = Location.table
        sm.userdata.destinations = [
            Destination(Location.table2),
            Destination(Location.table3),
        ]
        sm.userdata.num_placed = 0

        with sm:
            smach.StateMachine.add("_start", Reset(),
                                   transitions={
                                       "done": "check_dests",
            })
            smach.StateMachine.add("check_dests", CheckDestinations(),
                                   transitions={
                                       "failed": "done_failed",
                                       "succeeded": "reset",
            })
            smach.StateMachine.add("reset", Reset(),
                                   transitions={
                                       "done": "go_source",
            })
            smach.StateMachine.add("go_source", GoSource(),
                                   transitions={
                                       "failed": "reset",
                                       "succeeded": "find_object",
            })
            smach.StateMachine.add("find_object", FindObject(),
                                   transitions={
                                       "no_more_objects": "done_no_more_objects",
                                       "object_found": "pick_object",
            })
            smach.StateMachine.add("pick_object", PickObject(),
                                   transitions={
                                       "failed": "reset",
                                       "succeeded": "reset_while_holding",
            })
            smach.StateMachine.add("reset_while_holding", Reset(is_holding=True),
                                   transitions={
                                       "done": "go_dest",
            })
            smach.StateMachine.add("go_dest", GoDestination(),
                                   transitions={
                                       "failed": "done_failed",
                                       "succeeded": "find_place",
            })
            smach.StateMachine.add("find_place", FindPlace(),
                                   transitions={
                                       "failed": "done_failed",
                                       "succeeded": "place_object",
            })
            smach.StateMachine.add("place_object", PlaceObject(),
                                   transitions={
                                       "failed": "reset",
                                       "succeeded": "reset",
            })

        sm.execute()

        moveit_commander.roscpp_shutdown()
    except rospy.ROSException:
        rospy.loginfo("Quitting...")

import rospy
import tf2_ros

from rospy.exceptions import ROSException

from geometry_msgs.msg import Point, TransformStamped, Vector3
from object_recognition_msgs.msg import RecognizedObjectArray, TableArray
from tf2_geometry_msgs import do_transform_point, PointStamped


class ObjectRecognition:
    def __init__(self, objects_info):
        self.tf = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf)
        self.objects_info = objects_info

    def get_objects(self):
        output = []
        try:
            o = rospy.wait_for_message(
                "/recognized_object_array", RecognizedObjectArray, timeout=2)

            for obj in o.objects:
                info = self.objects_info[obj.type.key]

                size = info["size"]
                try:
                    center = self.pose_to_base(obj)

                    # center is front-bottom-left corner, apply offset to centroid
                    # don't need to offset x and y for some reason
                    # center.point.x += size[0] / 2
                    # center.point.y -= size[1] / 2
                    center.point.z += size[2] / 2

                    output.append({
                        "name": info["name"],
                        "center": center,
                        "size": size,
                        "confidence": obj.confidence
                    })
                except Exception as e:
                    rospy.logerr(e)
        except ROSException:
            rospy.logerr('Timed out while looking for objects')

        return output

    def pose_to_base(self, pose):
        point = PointStamped()
        point.header.frame_id = "xtion_rgb_optical_frame"
        point.header.stamp = rospy.Time(0)
        point.point = pose.pose.pose.pose.position

        return self.tf.transform(point, "base_footprint")

    def point_to_base(self, point):
        p = PointStamped()

        p.header.frame_id = "xtion_rgb_optical_frame"
        p.header.stamp = rospy.Time(0)
        p.point = point

        return self.tf.transform(p, "base_footprint").point

    def get_tables(self):
        tables = rospy.wait_for_message(
            "/table_array", TableArray, timeout=2).tables

        out = []

        for table in tables:
            rotate = table.pose.orientation
            center = table.pose.position

            def transform(p, r, c):
                v = PointStamped()
                v.point.x = p.x
                v.point.y = p.y
                v.point.z = p.z
                t = TransformStamped()
                t.transform.rotation = r
                t.transform.translation = c
                return do_transform_point(v, t).point

            hull = [self.point_to_base(transform(p, rotate, center))
                    for p in table.convex_hull]

            z_min = min(hull, key=lambda p: p.z).z
            z_max = max(hull, key=lambda p: p.z).z
            x_min = min(hull, key=lambda p: p.x).x
            x_max = max(hull, key=lambda p: p.x).x
            y_min = min(hull, key=lambda p: p.y).y
            y_max = max(hull, key=lambda p: p.y).y

            pointing_up = z_max - z_min < 0.1 and \
                x_max - x_min > 0.25 and \
                y_max - y_min > 0.25  # large flat surfaces

            if pointing_up:
                corner = Point()  # near right corner
                corner.x = x_min
                corner.y = y_min
                corner.z = z_max

                # assuming table is aligned to the global axes
                left = Vector3()  # to near left
                left.y = y_max - y_min

                forward = Vector3()  # to far right
                forward.x = x_max - x_min

                out.append([corner, left, forward])

        return out

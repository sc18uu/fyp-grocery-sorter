from math import cos, sin

import rospy

from actionlib import SimpleActionClient

from control_msgs.msg import PointHeadAction, PointHeadGoal
from geometry_msgs.msg import Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

from std_srvs.srv import Empty


class Navigation:
    def __init__(self):
        self.move = SimpleActionClient("/move_base", MoveBaseAction)
        self.head = SimpleActionClient(
            "/head_controller/point_head_action", PointHeadAction)

        self.move_velocity = rospy.Publisher(
            "/mobile_base_controller/cmd_vel", Twist)

        self.move.wait_for_server()

        self._clear_octomap = rospy.ServiceProxy('/clear_octomap', Empty)
        self._clear_costmap = rospy.ServiceProxy(
            '/move_base/clear_costmaps', Empty)

    def look_at(self, point):
        goal = PointHeadGoal()
        goal.target.header.stamp = rospy.Time.now()
        goal.target.header.frame_id = "/base_footprint"
        goal.target.point.x = point[0]
        goal.target.point.y = point[1]
        goal.target.point.z = point[2]
        goal.pointing_frame = "/xtion_rgb_optical_frame"
        goal.pointing_axis.x = 0
        goal.pointing_axis.y = 0
        goal.pointing_axis.z = 1
        goal.min_duration = rospy.Duration(0.5)
        goal.max_velocity = 0.5

        self.head.send_goal(goal)
        self.head.wait_for_result()

    def clear_map(self):
        self._clear_octomap.call()
        self._clear_costmap.call()

    def update_map(self):
        self.look_at((1, -0.75, 0.5))
        self.look_at((1, 0.75, 0.5))
        self.look_at((1, 0, 0.5))

    def go_to(self, pos):
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()

        goal.target_pose.pose.position.x = pos[0]
        goal.target_pose.pose.position.y = pos[1]
        goal.target_pose.pose.orientation.z = sin(pos[2] / 2)
        goal.target_pose.pose.orientation.w = cos(pos[2] / 2)

        self.move.send_goal(goal)
        wait = self.move.wait_for_result(rospy.Duration(35))
        if not wait:
            self.move.cancel_goal()
            return False
        else:
            return self.move.get_result()

    def go_velocity(self, x, time=1):
        goal = Twist()
        goal.linear.x = x

        stop = Twist()

        for i in range(time * 4):
            self.move_velocity.publish(goal)
            rospy.sleep(0.25)
        self.move_velocity.publish(stop)
